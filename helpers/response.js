const successResponse = (results, msg) => {
    return {
        success: true,
        msg: msg,
        result: results
    }
};

const errorResponse = (err) => {
    return {
        success: false,
        error: err
    }
};

module.exports = {
    successResponse,
    errorResponse
};
