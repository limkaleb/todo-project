const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const itemSchema = new Schema({
    content: {
        type: String,
        required: true
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        timestamps: true
    },
    completed: {
        type: Boolean,
        default: false,
        required: true
    },
    date: { type: Date, default: Date.now },
});

var Item = mongoose.model("Item", itemSchema);
module.exports = Item;
