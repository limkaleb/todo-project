var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcryptjs');
const saltRounds = 10;

var userSchema = new mongoose.Schema({
    // _id: { type: Schema.Types.ObjectId },
    username: {
        type: String,
        required: true,
        unique: true
    },
    hash: {
        type: String,
        required: true
    },
    email: {
        type: String, lowercase: true,
        match: [/\S+@\S+\.\S+/, 'is invalid'], index: true
    },
    firstname: {
        type: String
    },
    lastname: String,
    Timestamp: { type: Date, default: Date.now },

}, { collection: 'UsersSchema' });


userSchema.methods.validPassword = function (password, result) {
    const user = this;
    bcrypt.compare(password, user.hash)
        .then(() => {
            // return result = true;
            console.log("true");
        })
        .catch(() => {
            // return result = false;
            console.log("false");
        });
};


userSchema.methods.setPassword = function (password, callback) {
    try {
        var hash = bcrypt.hashSync(password, saltRounds);
        this.hash = hash;
    }
    catch{
        let error = new Error("Invalid Password")
        error.status = 400;
        return callback(error)
    }
};

userSchema.plugin(uniqueValidator, { message: 'Error, expected {PATH} to be unique.' });
module.exports = mongoose.model('User', userSchema);
