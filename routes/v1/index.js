var express = require('express');
var router = express.Router();
var usersRouter = require('./users');
var itemsRouter = require('./items');

router.use('/users', usersRouter);
router.use('/items', itemsRouter);

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Expresszz !!!' });
});

module.exports = router;
