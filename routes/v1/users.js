var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var userController = require('../../controllers/api/userController.js');

router.get('/', userController.Users);

router.get('/:username', userController.UsersSearchUsername);

router.post('/', userController.UsersInsert);

router.post('/login', userController.authentication);

router.put('/:id', userController.UsersUpdate);

router.delete('/:id', userController.UsersDelete);

module.exports = router;
