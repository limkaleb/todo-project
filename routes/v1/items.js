var express = require('express');
var router = express.Router();
const Item = require('../../models/Item');
const itemController = require('../../controllers/api/itemController');
const jwt = require('jsonwebtoken');
const auth = require('../../middlewares/auth');

/* GET users listing. */
router.get('/', auth.isAuthenticated, function (req, res, next) {
    res.send('This is item page');
});

// Get all items
router.get("/all", itemController.getAll);

// Get one item
router.get("/:content", itemController.getContent);

// Post items
router.post('/:username', itemController.postData);

// Update content by ID
router.put("/:contentId", itemController.updateContentById);

// Delete by ID
router.delete("/:id", itemController.delById);

// Delete all
router.delete("/", itemController.delAll);


module.exports = router;
