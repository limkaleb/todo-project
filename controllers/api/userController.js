var FuncHelpers = require('../../helpers/response.js');
var User = require('../../models/User.js');
var jwt = require('jsonwebtoken');

const bcrypt = require('bcryptjs');
const saltRounds = 10;

exports.Users = function (req, res, next) {
    User.find().exec()
        .then((users) => {
            res.status(201).json(FuncHelpers.successResponse(users));
        })
        .catch((err) => {
            res.status(422).json(FuncHelpers.errorResponse(users));
        });
}

exports.UsersSearchUsername = function (req, res, next) {
    let username = req.params.username;
    User.find({ "username": new RegExp(username, 'i') }).exec()
        .then((users) => {
            res.status(201).json(FuncHelpers.successResponse(users));
        })
        .catch((err) => {
            res.status(422).json(FuncHelpers.errorResponse(users));
        });
}

exports.UsersInsert = function (req, res) {
    user = new User({
        username: req.body.username,
        email: req.body.email
    })
    user.setPassword(req.body.password, (err) => {
        res.send(err)
    });
    user.save().then(function () {
        console.log('success');
        res.send('user created');

    }).catch((err) => {
        res.send(err);
    });

}

// exports.UsersInsert = function (req, res, next) {
//     bcrypt.hash(req.body.password, saltRounds, (err, hash) => {
//         if (err) {
//             return res.status(500).json({
//                 error: err
//             });
//         } else {
//             const user = new User({
//                 username: req.body.username,
//                 password: hash,
//                 email: req.body.email,
//                 firstname: req.body.firstname,
//                 lastname: req.body.lastname
//             });
//             user
//                 .save()
//                 .then(result => {
//                     console.log(result);
//                     res.status(201).json({
//                         message: 'User created'
//                     });
//                 })
//                 .catch(err => {
//                     console.log(err);
//                     res.status(500).json({
//                         error: err
//                     });
//                 })
//         }
//     })
// }

exports.UsersUpdate = function (req, res, next) {
    let id = req.params.id;
    let pengganti = req.body;
    User.findOneAndUpdate({ "_id": id }, pengganti).exec()
        .then((book) => {
            res.status(201).json({
                success: true,
                result: pengganti
            });
        })
        .catch((err) => {
            res.status(422).json({
                success: false,
                error: err
            });
        });
}

exports.UsersDelete = function (req, res) {
    let id = req.params.id;
    User.findByIdAndRemove(id, (err, users) => {
        if (err) return res.status(500).send(err);
        const response = {
            message: "Users successfully deleted",
            id: users._id
        };
        return res.status(200).send(response);
    });
}

exports.authentication = (req, res, next) => {
    let user = User.findOne({
        username: req.body.username
    }, function (err, obj) {
        if (!err) {
            return obj
        } else {
            next(err)
        }
    })
    user.then((user) => {
        // if (user.validPassword(req.body.password)) {
        if (bcrypt.compareSync(req.body.password, user.hash)) {
            // Taro JWT disini atau login untuk pasport juga boleh
            var token = jwt.sign(user.toJSON(), process.env.SECRET_KEY, { // melakukan generate token di jwt
                algorithm: 'HS256'
            });
            res.json({ message: 'berhasil login', token: token, success: true });
        } else {
            res.status(401)
            res.send({ Message: 'Password salah', success: false })
        }

    })
}