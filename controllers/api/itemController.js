const Item = require("../../models/Item");
const User = require("../../models/User");
const { successResponse, errorResponse } = require("../../helpers/response");

exports.getAll = function (req, res) {
    Item.find()
        .then((Item) => {
            res.json({ success: true, results: Item });
        }).catch((err) => {
            res.json({
                success: false,
                error: err
            });
        });
};

exports.postData = function (req, res) {
    User.findOne({ username: req.params.username }, (err, user) => {
        if (err) {
            res.status(422).json({ success: false, err: err })
        }
        let item = new Item({
            content: req.body.content,
            author: user._id
        })
        item.save((err) => {
            if (err) {
                res.status(422).json({ success: false, err: err, msg: "error after save" })
            }
            res.status(201).json({ results: item })
        })
    });
};

exports.updateContentById = function (req, res) {
    Item.updateOne({ _id: req.params.contentId },
        {
            $set: {
                content: req.body.content,
            }
        }
    )
        .then((Item) => {
            res.status(200).json(successResponse(Item, 'Successfully updated!'))
        }).catch((err) => {
            res.status(404).json(errorResponse(err))
        })
};

exports.delById = function (req, res) {
    Item.deleteOne({ _id: req.params.id })
        .then((Item) => {
            res.status(204)
        }).catch((err) => {
            res.status(404).json(errorResponse(err))
        })
};

exports.delAll = function (req, res) {
    Item.deleteMany({}, function (err) {
        if (err) {
            res.status(404).json(errorResponse(err))
        } else {
            res.status(200).json(successResponse(Item, 'Successfully deleted!'))
        }
    })
};

exports.getContent = function (req, res) {
    Item.findOne({ content: req.params.content }).populate('author')
        .exec()
        .then((Item) => {
            res.status(200).json(successResponse(Item, 'Successfully!'))
        })
        .catch((err) => {
            res.status(404).json(errorResponse(err))
        })
};
